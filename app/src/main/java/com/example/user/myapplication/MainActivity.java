package com.example.user.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private CheckBox ChkMarcame;
    private TextView msgText;
    private Button ButtonToastId;

    private LinearLayout ln;
    private SensorManager sm;
    private Sensor sensor;
    private TextView tv;

    private ListView lista;
    private String[] valores = new String[]{"Holanada","China","Rusia","Peru","Argentina","Francia"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChkMarcame = (CheckBox)findViewById(R.id.ChkMarcame);
        msgText = (TextView)findViewById(R.id.msgText);
        ButtonToastId = (Button)findViewById(R.id.ButtonToastId);


        /*Sensor*/
        ln = (LinearLayout) findViewById(R.id.linear);
        tv = (TextView) findViewById(R.id.msgText);
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        /*lista*/
        lista = (ListView) findViewById(R.id.lista);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,valores);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Posicion: "+position,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void toastMessage(View view) {
        Toast.makeText(getApplicationContext(),"Hola Antonio",Toast.LENGTH_SHORT).show();
    }

    public void checkMessage(View view) {
        boolean isChecked = ((CheckBox)view).isChecked();

        if (isChecked) {
            msgText.setText("Checkbox marcado!");
        }
        else {
            msgText.setText("Checkbox desmarcado!");
        }
    }

    public void siguienteActivity(View view) {
        Intent intent = new Intent(MainActivity.this,SegundaActivity.class);
        intent.putExtra("datox",msgText.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        String texto = String.valueOf(event.values[0]);
        tv.setText(texto);
        float valor = Float.parseFloat(texto);

        if(valor == 0){
            ln.setBackgroundColor(Color.BLUE);
        }
        else{
            ln.setBackgroundColor(Color.YELLOW);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
