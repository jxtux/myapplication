package com.example.user.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {
    private TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        texto = (TextView)findViewById(R.id.textViewIdx);

        //Recibir data de activity anterior
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if(extras != null ){
            String dato = extras.getString("datox");
            texto.setText(dato);
        }
    }
}
